'use stsrict';

class Mode {
	constructor () {
		this.timing = [0,0,0,0, 0,0,0,0];
		this.clock = 0;
		this.vfreq = 0;
		this.vsync = 0;
		this.hsync = 0;
		this.interlace = false;
	}

	get htotal() {
		return this.timing[0] + 
		       this.timing[1] + 
		       this.timing[2] + 
		       this.timing[3];
	}

	get vtotal() {
		return this.timing[4] + 
		       this.timing[5] + 
		       this.timing[6] + 
		       this.timing[7];
	}
	
	get hblank() {
		return this.timing[1] +
		       this.timing[2] +
		       this.timing[3];
	}

	get vblank() {
		return this.timing[5] +
		       this.timing[6] +
		       this.timing[7];
	}
	
	clock_from_vfreq() {
		return this.htotal * this.vtotal * this.vfreq / 1000000;
	}

	vfreq_from_clock() {
		let frame_us = (this.htotal*this.vtotal)/this.clock; 
		let hz = 1000000 / frame_us;
		return hz;
	}

	get hfreq () {
		return (this.vtotal * this.vfreq / 1000.0);
	}


	import_ui(ui) {
		for (let i = 0; i < 8; i++) {
			this.timing[i] = parseFloat(ui.timing[i].value);
		}

		this.vfreq = parseFloat(ui.vfreq.value);
		this.interlace = (ui.interlace.value == "true");
		if (this.interlace)
			this.vfreq /= 2.0;

		this.clock = parseFloat(ui.clock.value);
		if(ui.vpolarity.value == "positive") this.vsync = +1;
		if(ui.vpolarity.value == "negative") this.vsync = -1;
		if(ui.hpolarity.value == "positive") this.hsync = +1;
		if(ui.hpolarity.value == "negative") this.hsync = -1;
		return this;
	}

	import_modeline (modeline) {
		let values = [
			-1, 
			-1,-1,-1,-1,
			-1,-1,-1,-1
		];
		let flags = [];

		let split = modeline.split(" ");
		let t = 0;
		for (let i = 0; i < split.length; i++) {
			let s = split[i].trim();
			if (s == "Modeline") continue;
			if (s[0] == '"')  continue;
			if (s == '') continue;

			if (t < 9) {
				let num = parseFloat(s);
				values[t] = num;
				t++;
			} else {
				flags.push(s);
			}
		}

		for (let i = 0; i < 8; i++)
			this.timing[i] = values[i+1];
		
		/* Convert from absolute timing to relative */
		for (let i = 1; i < 8; i++) {
			if(i == 4) continue;
			this.timing[i] -= values[i];
		}
		
		for (let i = 0; i < flags.length; i++) {
			let s = flags[i];
			switch (s.toLowerCase()) {
				case "interlace":
					this.interlace = true;
					break;

				case "+hsync":
					this.hsync = +1;
					break;
				case "-hsync":
					this.hsync = -1;
					break;

				case "+vsync":
					this.vsync = +1;
					break;
				case "-vsync":
					this.vsync = -1;
					break;
			}
		}
		
		this.clock = values[0];
		this.vfreq = this.vfreq_from_clock();

		return this;
	}

	valid() {
		let min = [
			1,0,8,0,
			1,0,1,0
		];

		for (let i = 0; i < 8; i++) {
			if (isNaN(this.timing[i]) |
				this.timing[i] < min[i]) return false;
		}
		
		if (isNaN(this.clock)) return false;
		if (isNaN(this.vfreq)) return false;
		if (this.clock <= 0.0) return false;
		if (this.vfreq <= 0.0) return false;

		return true;
	}

	export_modeline () {
		let hz = this.vfreq;
		let name = `${this.timing[0]}x${this.timing[4]}_${hz.toFixed(2)}${(this.interlace)?"i":""}`
		let clock      = this.clock;

		let hdisp      = this.timing[0];
		let hsyncstart = hdisp      + this.timing[1];
		let hsyncend   = hsyncstart + this.timing[2]; 
		let htotal     = hsyncend   + this.timing[3]; 

		let vdisp      = this.timing[4];
		let vsyncstart = vdisp      + this.timing[5];
		let vsyncend   = vsyncstart + this.timing[6]; 
		let vtotal     = vsyncend   + this.timing[7]; 
		
		let flags = "";

		if (this.hsync > 0) flags += "+HSync ";
		if (this.hsync < 0) flags += "-HSync ";
		if (this.vsync > 0) flags += "+VSync ";
		if (this.vsync < 0) flags += "-VSync ";
		if (this.interlace) flags += "Interlace ";

		let modeline = `"${name}"  ${clock.toFixed(4)}  `+
		`  ${hdisp} ${hsyncstart} ${hsyncend} ${htotal} `+
		`  ${vdisp} ${vsyncstart} ${vsyncend} ${vtotal}   ${flags}`;
		return modeline.trim();

	}
}




class Editor {
	constructor() {
		this.canvas = document.querySelector("canvas");
		this.ctx = this.canvas.getContext("2d");

		this.modeline = document.querySelector("#modeline");

		this.modeline_clear = document.querySelector("button#clear");
		this.modeline_clear.onclick = () => {
			this.modeline.value = "";
		}

		this.timing = []

		for (let i = 1; i < 9; i++) {
			let input = document.querySelector("input#raw"+i);
			this.timing.push(input);
		}
		
		this.us = {
			hdisp  : document.querySelector("#us_hdisp"),
			hfront : document.querySelector("#us_hfront"),
			hsync  : document.querySelector("#us_hsync"),
			hback  : document.querySelector("#us_hback"),
			hblank : document.querySelector("#us_hblank"),
			htotal : document.querySelector("#us_htotal"),
			vdisp  : document.querySelector("#us_vdisp"),
			vfront : document.querySelector("#us_vfront"), 
			vsync  : document.querySelector("#us_vsync"),
			vback  : document.querySelector("#us_vback"),
			vblank : document.querySelector("#us_vblank"),
			vtotal : document.querySelector("#us_vtotal")
		};


		this.hstep = document.querySelector("select#hstep");
		this.scale = document.querySelector("input#scale");

		this.interlace = document.querySelector("select#interlace");
		this.vpolarity = document.querySelector("select#vpol");
		this.hpolarity = document.querySelector("select#hpol");

		this.vposition = document.querySelector("input#vpos");
		this.hposition = document.querySelector("input#hpos");



		this.resolution = document.querySelector("td#resolution");
		this.hfreq      = document.querySelector("td#hfreq");

		this.vfreq     = document.querySelector("input#vfreq");
		this.clock      = document.querySelector("input#clock");

		this.htotal     = document.querySelector("td#htotal");
		this.vtotal     = document.querySelector("td#vtotal");
		this.hblank     = document.querySelector("td#hblank");
		this.vblank     = document.querySelector("td#vblank");


		let mode = new Mode();
		mode.import_modeline(`"1024x768_60.00"  65.00  1024 1048 1184 1344  768 771 777 806 -HSync +VSync`);
		
		this.update(mode);

		/* Events */

		for (let i = 0; i < 8; i++) {
			this.timing[i].oninput = this.edit_event.bind(this);
		}
		this.interlace.oninput = this.edit_event.bind(this);
		this.vpolarity.oninput = this.edit_event.bind(this);
		this.hpolarity.oninput = this.edit_event.bind(this);
		this.vfreq.oninput     = this.edit_event.bind(this);

		this.vposition.oninput = this.position_edit_event.bind(this);
		this.hposition.oninput = this.position_edit_event.bind(this);
		this.hstep.oninput = this.edit_event.bind(this);
		this.scale.oninput = this.edit_event.bind(this);

		this.clock.oninput    = () => {
			let mode = new Mode().import_ui(this);
			mode.vfreq = mode.vfreq_from_clock();

			this.update(mode);
		}

		this.modeline.oninput = () => {
			let mode = new Mode().import_modeline(this.modeline.value);
			this.update(mode);
		}
	}

	edit_event() {
		let mode = new Mode().import_ui(this);
		mode.clock = mode.clock_from_vfreq();
		this.update(mode);
	}

	position_edit_event() {
		this.timing[3].value = this.hposition.value;
		this.timing[1].value = this.hposition.max - this.hposition.value;
		this.timing[7].value = this.vposition.value;
		this.timing[5].value = this.vposition.max - this.vposition.value;
		this.update(new Mode().import_ui(this));
	}


	update_position(mode) {
		this.hposition.min   = 0;
		this.hposition.max   = mode.timing[1]+mode.timing[3];
		this.hposition.value = mode.timing[3];
		this.hposition.step  = this.hstep.value;

		this.vposition.min   = 0;
		this.vposition.max   = mode.timing[5]+mode.timing[7];
		this.vposition.value = mode.timing[7];
	}

	update (mode) {
		if (!mode.valid()) {
			this.update_preview(mode);
			return;
		}

		let hz = mode.vfreq;
		this.resolution.innerHTML = `${mode.timing[0]}x${mode.timing[4]}${mode.interlace?"i":"p"} @ ${hz.toFixed(2) } Hz`;
		
		if (document.activeElement != this.clock)
			this.clock.value = mode.clock;

		if (document.activeElement != this.modeline)
			this.modeline.value = mode.export_modeline();
		
		this.hfreq.innerHTML = `${mode.hfreq.toFixed(4)} kHz`;

		this.htotal.innerHTML = mode.htotal;
		this.vtotal.innerHTML = mode.vtotal;
		this.hblank.innerHTML = mode.hblank;
		this.vblank.innerHTML = mode.vblank;


		/* Timing inputs */
		for (let i = 0; i < 8; i++) {
			if (i < 5) this.timing[i].step = this.hstep.value;
			this.timing[i].value = mode.timing[i];
		}

		this.vfreq.value = mode.vfreq * ((mode.interlace) ? 2 : 1);
		this.interlace.value = (mode.interlace) ? "true" : "false";

		this.hpolarity.value = "default"
		if(mode.hsync > 0) this.hpolarity.value = "positive";
		if(mode.hsync < 0) this.hpolarity.value = "negative";
		this.vpolarity.value = "default"
		if(mode.vsync > 0) this.vpolarity.value = "positive";
		if(mode.vsync < 0) this.vpolarity.value = "negative";

		/* µs times */
		let times = {};
		
		times.hdisp  = (1/mode.clock) * mode.timing[0];
		times.hfront = (1/mode.clock) * mode.timing[1];
		times.hsync  = (1/mode.clock) * mode.timing[2];
		times.hback  = (1/mode.clock) * mode.timing[3];

		times.hblank = (1/mode.clock) * mode.hblank;
		times.htotal = (1/mode.clock) * mode.htotal;

		times.vdisp  = times.htotal * mode.timing[4];
		times.vfront = times.htotal * mode.timing[5];
		times.vsync  = times.htotal * mode.timing[6];
		times.vback  = times.htotal * mode.timing[7];

		times.vtotal = times.htotal * mode.vtotal;
		times.vblank = times.htotal * mode.vblank;

		let p = 3;
		this.us.hdisp .innerHTML = `${times.hdisp.toFixed (p)} µs`
		this.us.hfront.innerHTML = `${times.hfront.toFixed(p)} µs`
		this.us.hsync .innerHTML = `${times.hsync.toFixed (p)} µs`
		this.us.hback .innerHTML = `${times.hback.toFixed (p)} µs`
		this.us.hblank.innerHTML = `${times.hblank.toFixed(p)} µs`
		this.us.htotal.innerHTML = `${times.htotal.toFixed(p)} µs`
		this.us.vdisp .innerHTML = `${times.vdisp.toFixed (p)} µs`
		this.us.vfront.innerHTML = `${times.vfront.toFixed(p)} µs`
		this.us.vsync .innerHTML = `${times.vsync.toFixed (p)} µs`
		this.us.vback .innerHTML = `${times.vback.toFixed (p)} µs`
		this.us.vblank.innerHTML = `${times.vblank.toFixed(p)} µs`
		this.us.vtotal.innerHTML = `${times.vtotal.toFixed(p)} µs`

		this.update_position(mode);
		this.update_preview(mode);
	}

	update_preview(mode){
		let ctx = this.ctx;

		if (!mode.valid()) {
			this.canvas.width  = 400;
			this.canvas.height = 400;

			ctx.fillStyle = "#FF8888";
			ctx.fillRect( 0, 0, this.canvas.width, this.canvas.height );

			ctx.font = "20px Serif";
			ctx.fillStyle = "#000000";
			ctx.textAlign = "center";

			ctx.fillText( "Invalid Timing", 
				this.canvas.width/2,
				this.canvas.height/2
			);

			return;
		}
		
		let scale = parseFloat(this.scale.value);

		let hdisp      = mode.timing[0];
		let hsyncstart = hdisp      + mode.timing[1];
		let hsyncend   = hsyncstart + mode.timing[2]; 
		let htotal     = hsyncend   + mode.timing[3]; 
		let vdisp      = mode.timing[4];
		let vsyncstart = vdisp      + mode.timing[5];
		let vsyncend   = vsyncstart + mode.timing[6]; 
		let vtotal     = vsyncend   + mode.timing[7]; 

		hdisp     *=scale;
		hsyncstart*=scale;
		hsyncend  *=scale;
		htotal    *=scale;
		vdisp     *=scale;
		vsyncstart*=scale;
		vsyncend  *=scale;
		vtotal    *=scale;

		let hfront = mode.timing[1] * scale;
		let hsync  = mode.timing[2] * scale;
		let hback  = mode.timing[3] * scale;
		let vfront = mode.timing[5] * scale;
		let vsync  = mode.timing[6] * scale;
		let vback  = mode.timing[7] * scale;

		this.canvas.width = htotal;
		this.canvas.height = vtotal;
		ctx.clearRect( 0, 0, this.canvas.width, this.canvas.height );

		ctx.fillStyle = "#8888FF";
		ctx.fillRect(0, 0, htotal, vtotal);

		ctx.fillStyle = "#FF8888";
		ctx.fillRect(0, 0, hsyncstart+hback, vsyncstart+vback );

		ctx.fillStyle = "#AAFFAA";
		ctx.fillRect(hback, vback, hdisp, vdisp );


		let hz = mode.vfreq;
		let res = `${mode.timing[0]}x${mode.timing[4]}${mode.interlace?"i":"p"} @ ${hz.toFixed(2) } Hz`;

		ctx.font = "20px Serif";
		ctx.fillStyle = "#000000";
		ctx.textAlign = "center"

		ctx.fillText( "Visible Area", hback+hdisp/2, vback+vdisp/2 )
		ctx.fillText( res, hback+hdisp/2, vback+vdisp/2+25 )
		
		/* V BACK */
		let fontsize = Math.min(vback, 20);
		ctx.font = `${fontsize}px Serif`;
		ctx.fillText( "V Back Porch", htotal/2, vback/2 + fontsize/4 );

		/* V FRONT */
		fontsize = Math.min(vfront, 20);
		ctx.font = `${fontsize}px Serif`;
		ctx.fillText( "V Front Porch", htotal /2, vback + vdisp + vfront/2 + fontsize/4 );

		/* V SYNC */
		fontsize = Math.min(vsync, 20);
		ctx.font = `${fontsize}px Serif`;
		ctx.fillText( "V Sync", htotal /2, vback+vdisp+vfront+vsync/2 + fontsize/4 );

		/* H FRONT */
		fontsize = Math.min(hfront, 20);
		ctx.font = `${fontsize}px Serif`;
		ctx.save()
		ctx.translate( hback+hdisp+hfront/2, vtotal/2 );
		ctx.rotate(Math.PI/2);
		ctx.fillText( "H Front Porch", 0,fontsize/4);
		ctx.restore()

		/* H SYNC */
		fontsize = Math.min(hsync, 20);
		ctx.font = `${fontsize}px Serif`;
		ctx.save()
		ctx.translate( hback+hdisp+hfront+hsync/2, vtotal/2 );
		ctx.rotate(Math.PI/2);
		ctx.fillText( "H Sync", 0, fontsize /4 );
		ctx.restore()

		/* H BACK */
		fontsize = Math.min(hback, 20);
		ctx.font = `${fontsize}px Serif`;
		ctx.save()
		ctx.translate( hback/2, vtotal/2 );
		ctx.rotate(Math.PI/2);
		ctx.fillText( "H Back Porch", 0,fontsize/4);
		ctx.restore()

	}
}

let editor = new Editor();
